#include "gl_renderer.h"
#include "gl_context.h"


GlRenderer::GlRenderer(HWND hwnd)
    : Renderer(hwnd)
{
    GLContext::Create(hwnd, 800, 600);
}

void GlRenderer::Tick()
{
    GLContext::ClearBackBuffer();
    // ... 
    GLContext::SwapContextBuffers();
}
