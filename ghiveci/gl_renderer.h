#pragma once
#include "renderer.h"


class GlRenderer : public Renderer
{
public:
    GlRenderer(HWND);

    void Tick() override;
};