// TODO support multiple windows 

#pragma once
#include <Windows.h>
#include <string>
#include <map>



//int WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int) {
//	Window window(hInstance, 1366, 768, "Dongles", false);
//	while (!window.ShouldClose()) {
//		window.RunMessageLoop();
//		// Do stuff
//	}
//}

class Window
{
public:
	Window(HINSTANCE, int width = 800, int height = 600, const std::wstring& caption = L"Window", bool bBorderless = false);
	~Window();

	// Always call ShouldClose() after running the message loop.
	void RunMessageLoop();
	const bool ShouldClose() const { return m_bShouldClose; }

	HWND GetHandle() const { return m_hWnd; }
	int  GetWidth()  const { return m_width; }
	int  GetHeight() const { return m_height; }

	void EnableCursor();
	void DisableCursor();

	void Clip();
	void Unclip();

private:
	HWND m_hWnd;
	DWORD m_windowStyle;
	int m_positionX;
	int m_positionY;
	int m_width;
	int m_height;
	bool m_bShouldClose;
	bool m_bCursorDisabled;

	static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
	static std::map<HWND, Window*> s_windows;
};