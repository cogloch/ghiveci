#pragma once
#include <Windows.h>


class Window;
class Renderer;

class App
{
public:
	App(HINSTANCE instance);
	~App();

	void Run();

private:
	Window* m_pWindow;
    Renderer* m_pRenderer;
};