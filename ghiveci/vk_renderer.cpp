#include "vk_renderer.h"
#include <set>
#include <array>
#include <vector>
#include <string>
#include <cassert>
#include <fstream>
#include "common.h"

#define VK_USE_PLATFORM_WIN32_KHR
#include <vulkan\vulkan.hpp>


struct VkRenderer::VkContext
{
    VkContext(HINSTANCE, HWND);
    ~VkContext();

    u32 width;
    u32 height;

    vk::Device device;
    vk::Instance instance;
    vk::PhysicalDevice physDevice;
    vk::PhysicalDeviceProperties physDeviceProperties;
    vk::PhysicalDeviceMemoryProperties physDeviceMemProperties;

#ifdef _DEBUG
    vk::DebugReportCallbackEXT dbgCallback;
#endif

    vk::Queue presentQueue;
    int presentQueueFamIdx;
    vk::Queue graphicsQueue;
    int graphicsQueueFamIdx;

    vk::SurfaceKHR surface;
    vk::SwapchainKHR swapchain;
    std::vector<vk::Image> presentImages;
    
    vk::Image depthImage;
    vk::ImageView depthImageView;

    vk::CommandBuffer drawCmdBuffer;
    vk::CommandBuffer setupCmdBuffer;

    vk::RenderPass renderPass;
    std::vector<vk::Framebuffer> framebufs;

    vk::Buffer vertBuffer;
    vk::Pipeline pipeline;
    vk::PipelineLayout pipelineLayout;

    bool CheckLayersAvailable(const std::vector<const char*>& neededLayers);
    bool CheckExtensionsAvailable(const std::vector<const char*>& neededExtensions);
};

#ifdef _DEBUG
PFN_vkCreateDebugReportCallbackEXT fpCreateDebugReportCallbackEXT = nullptr;
PFN_vkDestroyDebugReportCallbackEXT fpDestroyDebugReportCallbackEXT = nullptr;

VKAPI_ATTR VkResult VKAPI_CALL vkCreateDebugReportCallbackEXT(VkInstance instance, const VkDebugReportCallbackCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugReportCallbackEXT* pCallback) 
{
    return fpCreateDebugReportCallbackEXT(instance, pCreateInfo, pAllocator, pCallback);
}

VKAPI_ATTR void VKAPI_CALL vkDestroyDebugReportCallbackEXT(VkInstance instance, VkDebugReportCallbackEXT callback, const VkAllocationCallbacks* pAllocator) 
{
    fpDestroyDebugReportCallbackEXT(instance, callback, pAllocator);
}

VkBool32 __stdcall DebugReportCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objectType, u64 object, size_t location, int msgCode, const char* pLayerPrefix, const char* pMsg, void* pUserData)
{
    OutputDebugStringA(pLayerPrefix);
    OutputDebugStringA(" ");
    OutputDebugStringA(pMsg);
    OutputDebugStringA("\n");
    return VK_FALSE;
}
#endif

VkRenderer::VkContext::VkContext(HINSTANCE hInstance, HWND hwnd)
    : presentQueueFamIdx(-1)
    , graphicsQueueFamIdx(-1)
{
    std::vector<const char*> layers;
    std::vector<const char*> extensions{ "VK_KHR_surface", "VK_KHR_win32_surface" };
#ifdef _DEBUG
    layers.push_back("VK_LAYER_LUNARG_standard_validation");
    extensions.push_back("VK_EXT_debug_report");
#endif
    assert(CheckLayersAvailable(layers));
    assert(CheckExtensionsAvailable(extensions));

    // Create instance
    vk::InstanceCreateInfo instanceInfo({}, nullptr, layers.size(), layers.data(), extensions.size(), extensions.data());
    instance = vk::createInstance(instanceInfo);

    // Load extensions 
#ifdef _DEBUG
    fpCreateDebugReportCallbackEXT = (PFN_vkCreateDebugReportCallbackEXT)instance.getProcAddr("vkCreateDebugReportCallbackEXT");
    fpDestroyDebugReportCallbackEXT = (PFN_vkDestroyDebugReportCallbackEXT)instance.getProcAddr("vkDestroyDebugReportCallbackEXT");
#endif

    // Create debug callbacks 
#ifdef _DEBUG
    vk::DebugReportCallbackCreateInfoEXT callbackCreateInfo(vk::DebugReportFlagBitsEXT::eError | vk::DebugReportFlagBitsEXT::eWarning | vk::DebugReportFlagBitsEXT::ePerformanceWarning, &DebugReportCallback);
    dbgCallback = instance.createDebugReportCallbackEXT(callbackCreateInfo);
#endif

    // Create surface 
    vk::Win32SurfaceCreateInfoKHR surfaceCreateInfo({}, hInstance, hwnd);
    surface = instance.createWin32SurfaceKHR(surfaceCreateInfo);

    // Pick physical device 
    auto physDevices = instance.enumeratePhysicalDevices();
    bool found = false;
    for (auto& curPhysDevice : physDevices)
    {
        auto queueFamilies = curPhysDevice.getQueueFamilyProperties();

        for (u32 queueIdx = 0; queueIdx < queueFamilies.size(); ++queueIdx)
        {
            if (queueFamilies[queueIdx].queueCount == 0)
                continue;

            if (queueFamilies[queueIdx].queueFlags & vk::QueueFlagBits::eGraphics) // Supports graphics 
                graphicsQueueFamIdx = queueIdx;

            if (curPhysDevice.getSurfaceSupportKHR(queueIdx, surface)) // Supports present
                presentQueueFamIdx = queueIdx;

            if (graphicsQueueFamIdx != -1 && presentQueueFamIdx != -1) 
            {
                found = true;
                break;
            }
        }

        if (found)
        {
            physDevice = curPhysDevice;
            physDeviceProperties = curPhysDevice.getProperties();
            break;
        }
        else
        {
            presentQueueFamIdx = -1;
            graphicsQueueFamIdx = -1;
        }
    }

    physDeviceMemProperties = physDevice.getMemoryProperties();

    
    // Create logical device 
    float queuePriorities[] = { 1.f };
    std::set<int> queueFamIndices = { presentQueueFamIdx, graphicsQueueFamIdx };
    std::vector<vk::DeviceQueueCreateInfo> queueCreateInfos;
    for (int queueFamIdx : queueFamIndices)
        queueCreateInfos.push_back(vk::DeviceQueueCreateInfo({}, queueFamIdx, 1, queuePriorities));
    
    vk::PhysicalDeviceFeatures features = {};
    features.shaderClipDistance = VK_TRUE;
    
    const char* deviceExtensions[] = { "VK_KHR_swapchain" };
    vk::DeviceCreateInfo deviceCreateInfo({}, queueCreateInfos.size(), queueCreateInfos.data(), layers.size(), layers.data(), 1, deviceExtensions, &features);
    device = physDevice.createDevice(deviceCreateInfo);

    presentQueue = device.getQueue(presentQueueFamIdx, 0);
    graphicsQueue = device.getQueue(graphicsQueueFamIdx, 0);

    // Create command buffers
    vk::CommandPoolCreateInfo cmdPoolInfo(vk::CommandPoolCreateFlagBits::eResetCommandBuffer, presentQueueFamIdx);
    vk::CommandPool cmdPool = device.createCommandPool(cmdPoolInfo);
    vk::CommandBufferAllocateInfo cmdBufferAllocInfo(cmdPool, vk::CommandBufferLevel::ePrimary, 1);

    setupCmdBuffer = device.allocateCommandBuffers(cmdBufferAllocInfo)[0];
    drawCmdBuffer = device.allocateCommandBuffers(cmdBufferAllocInfo)[0];

    // Create swapchain
    auto surfaceFormats = physDevice.getSurfaceFormatsKHR(surface);
    vk::SurfaceFormatKHR surfaceFormat;
    if (surfaceFormats.size() == 1 && surfaceFormats[0].format == vk::Format::eUndefined)
    {
        surfaceFormat = { vk::Format::eB8G8R8A8Unorm, vk::ColorSpaceKHR::eSrgbNonlinear };
    }
    else
    {
        for (auto& curFormat : surfaceFormats)
        {
            if (curFormat.format == vk::Format::eB8G8R8A8Unorm && curFormat.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear)
            {
                surfaceFormat = curFormat;
                break;
            }
        }
    }

    vk::SurfaceCapabilitiesKHR surfaceCapabilities = physDevice.getSurfaceCapabilitiesKHR(surface);
    
    u32 chosenImageCount = 2;
    if (chosenImageCount < surfaceCapabilities.minImageCount)
    {
        chosenImageCount = surfaceCapabilities.minImageCount;
    }
    else if (surfaceCapabilities.maxImageCount == 0 && chosenImageCount > surfaceCapabilities.maxImageCount)
    {
        chosenImageCount = surfaceCapabilities.maxImageCount;
    }

    vk::Extent2D surfaceResolution = surfaceCapabilities.currentExtent;
    if (surfaceResolution.width == -1)
    {
        surfaceResolution.width = width;
        surfaceResolution.height = height;
    }
    else
    {
        width = surfaceResolution.width;
        height = surfaceResolution.height;
    }

    auto preTransform = surfaceCapabilities.currentTransform;
    if (surfaceCapabilities.supportedTransforms & vk::SurfaceTransformFlagBitsKHR::eIdentity)
    {
        preTransform = vk::SurfaceTransformFlagBitsKHR::eIdentity;
    }

    auto presentModes = physDevice.getSurfacePresentModesKHR(surface);

    vk::PresentModeKHR chosenPresentMode = vk::PresentModeKHR::eFifo;
    for (auto& presentMode : presentModes)
    {
        if (presentMode == vk::PresentModeKHR::eMailbox)
        {
            chosenPresentMode = presentMode;
            break;
        }
        else
        {
            chosenPresentMode = vk::PresentModeKHR::eImmediate;
        }
    }

    vk::SwapchainCreateInfoKHR swapchainCreateInfo({}, surface, chosenImageCount, surfaceFormat.format, surfaceFormat.colorSpace, surfaceResolution, 1, vk::ImageUsageFlagBits::eColorAttachment, vk::SharingMode::eExclusive, 0, nullptr, preTransform, vk::CompositeAlphaFlagBitsKHR::eOpaque, chosenPresentMode, VK_TRUE, VK_NULL_HANDLE);
    u32 queueFamiliesArr[2] = { graphicsQueueFamIdx, presentQueueFamIdx };
    if (graphicsQueueFamIdx != presentQueueFamIdx)
    {
        swapchainCreateInfo.imageSharingMode = vk::SharingMode::eConcurrent;
        swapchainCreateInfo.queueFamilyIndexCount = 2;
        swapchainCreateInfo.pQueueFamilyIndices = queueFamiliesArr;
    }
    swapchain = device.createSwapchainKHR(swapchainCreateInfo);

    // Create image views 
    presentImages = device.getSwapchainImagesKHR(swapchain);
    const u32 imageCount = presentImages.size();

    vk::ImageSubresourceRange resourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1);
    vk::ImageViewCreateInfo presentImagesViewCreateInfo({}, {}, vk::ImageViewType::e2D, surfaceFormat.format,
    { vk::ComponentSwizzle::eR, vk::ComponentSwizzle::eG, vk::ComponentSwizzle::eB, vk::ComponentSwizzle::eA }, resourceRange);

    vk::CommandBufferBeginInfo beginInfo(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
    vk::Fence submitFence = device.createFence({});

    std::vector<bool> transitioned(imageCount);
    u32 doneCount = 0;
    while (doneCount != imageCount)
    {
        vk::Semaphore presentCompleteSemaphore = device.createSemaphore({});
        u32 nextImageIdx = device.acquireNextImageKHR(swapchain, UINT64_MAX, presentCompleteSemaphore, VK_NULL_HANDLE).value;
        if (!transitioned[nextImageIdx])
        {
            setupCmdBuffer.begin(beginInfo);

            vk::ImageMemoryBarrier layoutTransitionBarrier({}, vk::AccessFlagBits::eMemoryRead, vk::ImageLayout::eUndefined, vk::ImageLayout::ePresentSrcKHR, VK_QUEUE_FAMILY_IGNORED, VK_QUEUE_FAMILY_IGNORED, presentImages[nextImageIdx], resourceRange);
            setupCmdBuffer.pipelineBarrier(vk::PipelineStageFlagBits::eTopOfPipe, vk::PipelineStageFlagBits::eTopOfPipe, {}, 0, nullptr, 0, nullptr, 1, &layoutTransitionBarrier);

            setupCmdBuffer.end();

            vk::PipelineStageFlags waitStageMask[] = { vk::PipelineStageFlagBits::eColorAttachmentOutput };
            vk::SubmitInfo submitInfo(1, &presentCompleteSemaphore, waitStageMask, 1, &setupCmdBuffer, 0, nullptr);
            presentQueue.submit(1, &submitInfo, submitFence);

            device.waitForFences(1, &submitFence, VK_TRUE, UINT64_MAX);
            device.resetFences(1, &submitFence);

            device.destroySemaphore(presentCompleteSemaphore);

            setupCmdBuffer.reset({});

            transitioned[nextImageIdx] = true;
            doneCount++;
        }

        vk::PresentInfoKHR presentInfo(0, nullptr, 1, &swapchain, &nextImageIdx);
        presentQueue.presentKHR(presentInfo);
    }

    std::vector<vk::ImageView> presentImageViews(imageCount);
    for (u32 i = 0; i < presentImages.size(); ++i)
    {
        presentImagesViewCreateInfo.image = presentImages[i];
        presentImageViews[i] = device.createImageView(presentImagesViewCreateInfo);
    }

    // Create depth buffer 
    vk::ImageCreateInfo imageCreateInfo({}, vk::ImageType::e2D, vk::Format::eD16Unorm, { width, height, 1 }, 1, 1, vk::SampleCountFlagBits::e1, vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eDepthStencilAttachment);
    depthImage = device.createImage(imageCreateInfo);

    vk::MemoryRequirements memRequirements = device.getImageMemoryRequirements(depthImage);
    vk::MemoryAllocateInfo imageAllocInfo(memRequirements.size);

    u32 memTypeBits = memRequirements.memoryTypeBits;
    vk::MemoryPropertyFlags desiredMemFlags = vk::MemoryPropertyFlagBits::eDeviceLocal;
    for (u32 i = 0; i < 32; ++i)
    {
        vk::MemoryType memType = physDeviceMemProperties.memoryTypes[i];
        if (memTypeBits & 1)
        {
            if ((memType.propertyFlags & desiredMemFlags) == desiredMemFlags)
            {
                imageAllocInfo.memoryTypeIndex = i;
                break;
            }
        }

        memTypeBits = memTypeBits >> 1;
    }

    vk::DeviceMemory imageMem = device.allocateMemory(imageAllocInfo);
    device.bindImageMemory(depthImage, imageMem, 0);

    setupCmdBuffer.begin(beginInfo);

    vk::ImageMemoryBarrier layoutTransitionBarrier({}, vk::AccessFlagBits::eDepthStencilAttachmentRead | vk::AccessFlagBits::eDepthStencilAttachmentWrite, vk::ImageLayout::eUndefined, vk::ImageLayout::eDepthStencilAttachmentOptimal, VK_QUEUE_FAMILY_IGNORED, VK_QUEUE_FAMILY_IGNORED, depthImage, { vk::ImageAspectFlagBits::eDepth, 0, 1, 0, 1 });
    setupCmdBuffer.pipelineBarrier(vk::PipelineStageFlagBits::eTopOfPipe, vk::PipelineStageFlagBits::eTopOfPipe, {}, 0, nullptr, 0, nullptr, 1, &layoutTransitionBarrier);

    setupCmdBuffer.end();

    vk::PipelineStageFlags waitStageMask[] = { vk::PipelineStageFlagBits::eColorAttachmentOutput };
    vk::SubmitInfo submitInfo(0, nullptr, waitStageMask, 1, &setupCmdBuffer, 0, nullptr);
    presentQueue.submit(1, &submitInfo, submitFence);

    device.waitForFences(1, &submitFence, VK_TRUE, UINT64_MAX);
    device.resetFences(1, &submitFence);
    setupCmdBuffer.reset({});

    vk::ImageViewCreateInfo imageViewCreateInfo({}, depthImage, vk::ImageViewType::e2D, imageCreateInfo.format, { vk::ComponentSwizzle::eIdentity, vk::ComponentSwizzle::eIdentity, vk::ComponentSwizzle::eIdentity, vk::ComponentSwizzle::eIdentity }, { vk::ImageAspectFlagBits::eDepth, 0, 1, 0, 1 });
    depthImageView = device.createImageView(imageViewCreateInfo);

    // Create render pass
    vk::AttachmentDescription colorAttach({}, surfaceFormat.format, vk::SampleCountFlagBits::e1, vk::AttachmentLoadOp::eClear, vk::AttachmentStoreOp::eStore, vk::AttachmentLoadOp::eDontCare, vk::AttachmentStoreOp::eDontCare, vk::ImageLayout::eColorAttachmentOptimal, vk::ImageLayout::eColorAttachmentOptimal);
    vk::AttachmentDescription depthAttach({}, vk::Format::eD16Unorm, vk::SampleCountFlagBits::e1, vk::AttachmentLoadOp::eClear, vk::AttachmentStoreOp::eDontCare, vk::AttachmentLoadOp::eDontCare, vk::AttachmentStoreOp::eDontCare, vk::ImageLayout::eDepthStencilAttachmentOptimal, vk::ImageLayout::eDepthStencilAttachmentOptimal);
    vk::AttachmentDescription attachments[2] = { colorAttach, depthAttach };

    vk::AttachmentReference colorAttachRef(0, vk::ImageLayout::eColorAttachmentOptimal);
    vk::AttachmentReference depthAttachRef(1, vk::ImageLayout::eDepthStencilAttachmentOptimal);

    vk::SubpassDescription subpass({}, vk::PipelineBindPoint::eGraphics, 0, nullptr, 1, &colorAttachRef, nullptr, &depthAttachRef);
    vk::RenderPassCreateInfo renderPassCreateInfo({}, 2, attachments, 1, &subpass);
    renderPass = device.createRenderPass(renderPassCreateInfo);

    // Create framebufs
    vk::ImageView framebufAttachments[2];
    framebufAttachments[1] = depthImageView;

    vk::FramebufferCreateInfo framebufCreateInfo({}, renderPass, 2, framebufAttachments, width, height, 1);
    framebufs.resize(imageCount);
    for (u32 i = 0; i < imageCount; ++i)
    {
        framebufAttachments[0] = presentImageViews[i];
        framebufs[i] = device.createFramebuffer(framebufCreateInfo);
    }

    // Create vertex bufs
    struct Vertex
    {
        float x, y, z, w;
    };

    vk::BufferCreateInfo vertBufferInfo({}, sizeof(Vertex) * 3, vk::BufferUsageFlagBits::eVertexBuffer);
    vertBuffer = device.createBuffer(vertBufferInfo);

    vk::MemoryRequirements vertBufMemReq = device.getBufferMemoryRequirements(vertBuffer);
    vk::MemoryAllocateInfo vertBufAllocInfo(vertBufMemReq.size);

    u32 vertMemTypeBits = vertBufMemReq.memoryTypeBits;
    vk::MemoryPropertyFlags vertDesiredMemFlags = vk::MemoryPropertyFlagBits::eHostVisible;
    for (u32 i = 0; i < 32; ++i)
    {
        vk::MemoryType memType = physDeviceMemProperties.memoryTypes[i];
        if (vertMemTypeBits & 1)
        {
            if ((memType.propertyFlags & vertDesiredMemFlags) == vertDesiredMemFlags)
            {
                vertBufAllocInfo.memoryTypeIndex = i;
                break;
            }
        }

        vertMemTypeBits = vertMemTypeBits >> 1;
    }

    vk::DeviceMemory vertBufMem = device.allocateMemory(vertBufAllocInfo);

    void* mapped = device.mapMemory(vertBufMem, 0, VK_WHOLE_SIZE, {});
    Vertex* triangle = (Vertex*)mapped;
    Vertex v1 = { -1.f, -1.f, 0.f, 1.f };
    Vertex v2 = {  1.f, -1.f, 0.f, 1.f };
    Vertex v3 = {  0.f,  1.f, 0.f, 1.f };
    triangle[0] = v1;
    triangle[1] = v2;
    triangle[2] = v3;

    device.unmapMemory(vertBufMem);

    device.bindBufferMemory(vertBuffer, vertBufMem, 0);

    // Create shaders
    std::ifstream vertFile("shaders/vert.spv");
    std::string vertShader;
    vertFile.seekg(0, std::ios::end);
    vertShader.resize(vertFile.tellg());
    vertFile.seekg(0, std::ios::beg);
    vertFile.read(&vertShader[0], vertShader.size());
    
    vk::ShaderModuleCreateInfo vertShaderCreateInfo({}, vertShader.size(), (u32*)vertShader.data());
    vk::ShaderModule vertShaderModule = device.createShaderModule(vertShaderCreateInfo);

    std::ifstream fragFile("shaders/frag.spv");
    std::string fragShader;
    fragFile.seekg(0, std::ios::end);
    fragShader.resize(fragFile.tellg());
    fragFile.seekg(0, std::ios::beg);
    fragFile.read(&fragShader[0], fragShader.size());
    
    vk::ShaderModuleCreateInfo fragShaderCreateInfo({}, fragShader.size(), (u32*)fragShader.data());
    vk::ShaderModule fragShaderModule = device.createShaderModule(fragShaderCreateInfo);

    // Create pipeline
    pipelineLayout = device.createPipelineLayout({});

    vk::PipelineShaderStageCreateInfo vertStageInfo({}, vk::ShaderStageFlagBits::eVertex, vertShaderModule, "main");
    vk::PipelineShaderStageCreateInfo fragStageInfo({}, vk::ShaderStageFlagBits::eFragment, fragShaderModule, "main");
    vk::PipelineShaderStageCreateInfo shaderStageCreateInfo[2] = { vertStageInfo, fragStageInfo };

    vk::VertexInputBindingDescription vertBindingDesc(0, sizeof(Vertex), vk::VertexInputRate::eVertex);
    vk::VertexInputAttributeDescription vertAttrDesc(0, 0, vk::Format::eR32G32B32A32Sfloat, 0);
    vk::PipelineVertexInputStateCreateInfo vertInputStateInfo({}, 1, &vertBindingDesc, 1, &vertAttrDesc);

    vk::PipelineInputAssemblyStateCreateInfo inputAssState({}, vk::PrimitiveTopology::eTriangleList, VK_FALSE);

    vk::Viewport viewport(0.f, 0.f, width, height, 0.f, 1.f);
    vk::Rect2D scissors({ 0, 0 }, { width, height });
    vk::PipelineViewportStateCreateInfo viewportStateInfo({}, 1, &viewport, 1, &scissors);

    vk::PipelineRasterizationStateCreateInfo rasterizationState({}, VK_FALSE, VK_FALSE, vk::PolygonMode::eFill, vk::CullModeFlagBits::eNone, vk::FrontFace::eCounterClockwise, VK_FALSE, 0, 0, 0, 1);
    
    vk::PipelineMultisampleStateCreateInfo multisampleState;

    vk::StencilOpState stencilState(vk::StencilOp::eKeep, vk::StencilOp::eKeep, vk::StencilOp::eKeep, vk::CompareOp::eAlways);
    vk::PipelineDepthStencilStateCreateInfo depthState({}, VK_TRUE, VK_TRUE, vk::CompareOp::eLessOrEqual, VK_FALSE, VK_FALSE, stencilState, stencilState);

    vk::PipelineColorBlendAttachmentState colorBlendAttachState(VK_FALSE, vk::BlendFactor::eSrcColor, vk::BlendFactor::eOneMinusDstColor);
    vk::PipelineColorBlendStateCreateInfo colorBlendState({}, VK_FALSE, vk::LogicOp::eClear, 1, &colorBlendAttachState, { 0.f, 0.f, 0.f, 0.f });

    vk::GraphicsPipelineCreateInfo pipelineCreateInfo({}, 2, shaderStageCreateInfo, &vertInputStateInfo, &inputAssState, nullptr, &viewportStateInfo, &rasterizationState, &multisampleState, &depthState, &colorBlendState, nullptr, pipelineLayout, renderPass, 0, nullptr, 0);
    pipeline = device.createGraphicsPipeline(VK_NULL_HANDLE, pipelineCreateInfo);
}

VkRenderer::VkContext::~VkContext()
{
    device.waitIdle();
    device.destroy();

#ifdef _DEBUG
    instance.destroyDebugReportCallbackEXT(dbgCallback);
#endif
    instance.destroySurfaceKHR(surface);
    instance.destroy();
}

bool VkRenderer::VkContext::CheckLayersAvailable(const std::vector<const char*>& neededLayers)
{
    auto presentLayers = vk::enumerateInstanceLayerProperties();

    u32 foundCount = 0;
    for(auto& presentLayer : presentLayers)
        for (auto& neededLayer : neededLayers)
            if (strcmp(presentLayer.layerName, neededLayer) == 0)
                foundCount++;

    return (neededLayers.size() == foundCount);
}

bool VkRenderer::VkContext::CheckExtensionsAvailable(const std::vector<const char*>& neededExtensions)
{
    auto presentExtensions = vk::enumerateInstanceExtensionProperties();

    u32 foundCount = 0;
    for (auto& presentExtension : presentExtensions)
        for (auto& neededExtension : neededExtensions)
            if (strcmp(presentExtension.extensionName, neededExtension) == 0)
                foundCount++;

    return (neededExtensions.size() == foundCount);
}


VkRenderer::VkRenderer(HWND hwnd, HINSTANCE hInstance)
    : Renderer(hwnd)
{
    m_pContext = new VkContext(hInstance, hwnd);
}

VkRenderer::~VkRenderer()
{
    delete m_pContext;
}

void VkRenderer::Tick()
{
    vk::Semaphore presentCompleteSemaphore = m_pContext->device.createSemaphore({});
    vk::Semaphore renderingCompleteSemaphore = m_pContext->device.createSemaphore({});

    u32 nextImageIdx = m_pContext->device.acquireNextImageKHR(m_pContext->swapchain, UINT64_MAX, presentCompleteSemaphore, VK_NULL_HANDLE).value;

    m_pContext->drawCmdBuffer.begin({ vk::CommandBufferUsageFlagBits::eOneTimeSubmit });

    vk::ImageMemoryBarrier layoutTransitionBarrier(vk::AccessFlagBits::eMemoryRead, vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite, vk::ImageLayout::ePresentSrcKHR, vk::ImageLayout::eColorAttachmentOptimal, VK_QUEUE_FAMILY_IGNORED, VK_QUEUE_FAMILY_IGNORED, m_pContext->presentImages[nextImageIdx], { vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1 });

    m_pContext->drawCmdBuffer.pipelineBarrier(vk::PipelineStageFlagBits::eTopOfPipe, vk::PipelineStageFlagBits::eTopOfPipe, {}, 0, nullptr, 0, nullptr, 1, &layoutTransitionBarrier);

    vk::ClearValue clearValues[2] = { vk::ClearColorValue(std::array<float, 4>{1.f, 1.f, 1.f, 1.f }), vk::ClearDepthStencilValue(1.f, 0.f)};
    vk::RenderPassBeginInfo renderPassBeginInfo(m_pContext->renderPass, m_pContext->framebufs[nextImageIdx], { { 0, 0 }, { m_pContext->width, m_pContext->height } }, 2, clearValues);
    m_pContext->drawCmdBuffer.beginRenderPass(renderPassBeginInfo, vk::SubpassContents::eInline);

    m_pContext->drawCmdBuffer.bindPipeline(vk::PipelineBindPoint::eGraphics, m_pContext->pipeline);

    vk::DeviceSize offsets = {};
    m_pContext->drawCmdBuffer.bindVertexBuffers(0, 1, &m_pContext->vertBuffer, &offsets);

    m_pContext->drawCmdBuffer.draw(3, 1, 0, 0);

    m_pContext->drawCmdBuffer.endRenderPass();

    vk::ImageMemoryBarrier prePresentBarrier(vk::AccessFlagBits::eColorAttachmentWrite, vk::AccessFlagBits::eColorAttachmentRead, vk::ImageLayout::eColorAttachmentOptimal, vk::ImageLayout::ePresentSrcKHR, VK_QUEUE_FAMILY_IGNORED, VK_QUEUE_FAMILY_IGNORED, m_pContext->presentImages[nextImageIdx], { vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1 });

    m_pContext->drawCmdBuffer.pipelineBarrier(vk::PipelineStageFlagBits::eAllCommands, vk::PipelineStageFlagBits::eBottomOfPipe, {}, 0, nullptr, 0, nullptr, 1, &prePresentBarrier);

    m_pContext->drawCmdBuffer.end();

    vk::Fence renderFence = m_pContext->device.createFence({});

    vk::PipelineStageFlags waitStageMask = { vk::PipelineStageFlagBits::eBottomOfPipe };
    vk::SubmitInfo submitInfo(1, &presentCompleteSemaphore, &waitStageMask, 1, &m_pContext->drawCmdBuffer, 1, &renderingCompleteSemaphore);
    m_pContext->presentQueue.submit(1, &submitInfo, renderFence);

    m_pContext->device.waitForFences(1, &renderFence, VK_TRUE, UINT64_MAX);
    m_pContext->device.destroyFence(renderFence);

    vk::PresentInfoKHR presentInfo(1, &renderingCompleteSemaphore, 1, &m_pContext->swapchain, &nextImageIdx);
    m_pContext->presentQueue.presentKHR(presentInfo);

    m_pContext->device.destroySemaphore(presentCompleteSemaphore);
    m_pContext->device.destroySemaphore(renderingCompleteSemaphore);
}