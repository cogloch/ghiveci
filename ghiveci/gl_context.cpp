#include "gl_context.h"
#include "gl.h"
#include "wgl.h"


HWND  GLContext::s_hWindow = nullptr;
HDC   GLContext::s_hDeviceContext = nullptr;
HGLRC GLContext::s_hRenderContext = nullptr;


void GLContext::Destroy()
{
	wglMakeCurrent(s_hDeviceContext, 0);
	wglDeleteContext(s_hRenderContext);
	ReleaseDC(s_hWindow, s_hDeviceContext);
}

void GLContext::Create(HWND hwnd, int width, int height, int majorVersion, int minorVersion)
{
	s_hWindow = hwnd;
	s_hDeviceContext = GetDC(hwnd);

	// Create pixel format
	PIXELFORMATDESCRIPTOR pixelFormatDesc;
	memset(&pixelFormatDesc, 0, sizeof(PIXELFORMATDESCRIPTOR));
	pixelFormatDesc.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
	pixelFormatDesc.iPixelType = PFD_TYPE_RGBA;
	pixelFormatDesc.cColorBits = 32;
	pixelFormatDesc.cDepthBits = 32;
	pixelFormatDesc.iLayerType = PFD_MAIN_PLANE;

	auto pixelFormat = ChoosePixelFormat(s_hDeviceContext, &pixelFormatDesc);
	SetPixelFormat(s_hDeviceContext, pixelFormat, &pixelFormatDesc);

	// Create a temporary Opengl 2.1 context to allow the loading of functions that can create a 4.5 context
	auto tempContext = wglCreateContext(s_hDeviceContext);
	wglMakeCurrent(s_hDeviceContext, tempContext);

	// No explicit init required for function loading; only for checking available extensions
	gl::sys::CheckExtensions();

	// If the context creation extension is available, create an Opengl 4.5 context and make it active(replacing the temporary one)
	// If not, keep using the 2.1 context
	if (wgl::exts::var_ARB_create_context)
	{
		int attributes[] = {
			wgl::CONTEXT_MAJOR_VERSION_ARB, majorVersion,
			wgl::CONTEXT_MINOR_VERSION_ARB, minorVersion,
			wgl::CONTEXT_FLAGS_ARB, wgl::CONTEXT_CORE_PROFILE_BIT_ARB,
			0
		};

		s_hRenderContext = wgl::CreateContextAttribsARB(s_hDeviceContext, nullptr, attributes);

		wglDeleteContext(tempContext);
		wglMakeCurrent(s_hDeviceContext, s_hRenderContext);
	}
	else
	{
		// TODO: Decide if crash or allow using the 2.1 context
		// DestroyContext();
		s_hRenderContext = tempContext;
	}

	gl::ClearColor(0.4f, 0.6f, 0.9f, 1.0f);
	gl::Viewport(0, 0, width, height);
}


void GLContext::SwapContextBuffers()
{
	SwapBuffers(s_hDeviceContext);
}

void GLContext::ClearBackBuffer(GLbitfield flags)
{
	gl::Clear(flags);
}

void GLContext::ClearBackBuffer(float r, float g, float b, float a)
{
	gl::ClearColor(r, g, b, a);
	ClearBackBuffer();
}

void GLContext::ClearBackBuffer(GLbitfield flags, float r, float g, float b, float a)
{
	gl::ClearColor(r, g, b, a);
	ClearBackBuffer(flags);
}

void GLContext::Resize(int width, int height)
{
	gl::Viewport(0, 0, width, height);
}