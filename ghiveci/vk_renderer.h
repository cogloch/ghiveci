#pragma once
#include "renderer.h"


class VkRenderer : public Renderer
{
public:
    VkRenderer(HWND hwnd, HINSTANCE hInstance);
    ~VkRenderer();

    void Tick() override;

private:
    struct VkContext;
    VkContext* m_pContext;
};