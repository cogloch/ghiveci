// Project properties 
//      <Additional Include Directories> $(ProjectDir)include\
//      <Additional Include Directories> $(VULKAN_SDK)\Include\
//      <Additional Include Directories> $(VULKAN_SDK)\Third-Party\Include\
//      <Additional Library Directories>(x64) $(VULKAN_SDK)\Lib\
//      <Additional Library Directories>(x86) $(VULKAN_SDK)\Lib32\
// Libs 
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "vulkan-1.lib")
#include "app.h"


int __stdcall WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int)
{
	App app(hInstance);
	app.Run();
}