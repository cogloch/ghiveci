#include "app.h"
#include "win32_window.h"
#include "gl_renderer.h"
#include "vk_renderer.h"


App::App(HINSTANCE instance)
{
    m_pWindow = new Window(instance);
    //m_pRenderer = new GlRenderer(m_pWindow->GetHandle());
    m_pRenderer = new VkRenderer(m_pWindow->GetHandle(), instance);
}

App::~App()
{
    delete m_pWindow;
    delete m_pRenderer;
}

void App::Run()
{
	auto hwnd = m_pWindow->GetHandle();
	while (!m_pWindow->ShouldClose())
	{
		m_pWindow->RunMessageLoop();

		// Update
		// ... 

		//Render
        m_pRenderer->Tick();
	}
}
