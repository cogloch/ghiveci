// TODO support multiple contexts

#pragma once
#include <Windows.h>
#include "gl.h"


class GLContext
{
public:
	static void Create(HWND, int width, int height, int majorVersion = 4, int minorVersion = 5);
	static void Destroy();

	static void ClearBackBuffer(GLbitfield flags = gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT | gl::STENCIL_BUFFER_BIT);
	static void ClearBackBuffer(float r, float g, float b, float a);
	static void ClearBackBuffer(GLbitfield flags, float r, float g, float b, float a);
	static void SwapContextBuffers();

	static void Resize(int width, int height);

	GLContext() = delete;

private:
	static HWND  s_hWindow;
	static HDC   s_hDeviceContext;
	static HGLRC s_hRenderContext;
};