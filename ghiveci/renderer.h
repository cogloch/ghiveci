#pragma once
#include <Windows.h>


class Renderer
{
public:
    virtual ~Renderer();

    virtual void Tick() = 0;

protected:
    Renderer(HWND);
};